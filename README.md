# Overview #

You will be creating a web application in the language/framework of your choice that plays the game "Connect Four” 
interactively (Human vs Computer). [Here](https://en.wikipedia.org/wiki/Connect_Four) is a great overview of the game. 

##### Level 1 #####
Create a github repo with a stubbed out “Hello world” web application in the framework of your choice and get it deployed up to Heroku (or similar).  

##### Level 2 #####
Create the models that represent a Connect Four game that would allow games to be created and 2 users to place coins on the board.

##### Level 3 #####
Implement a user interface in HTML & CSS (and preferably in a SPA Javascript framework) that represents the board and the positions of coins on that board.

##### Level 4 #####
Implement a turn-based gameplay system that:
* decides which player goes first (can be random or user-determined). 
* clearly indicates whose turn it is (player vs computer).  
* alternates players’ turns
* keeps track of the results of each play
* Computer player can place coins completely randomly or however you’d like.

##### Level 5 #####
Add win conditions based on the rules of Connect Four.
* Vertical
* Horizontal
* Diagonal

##### Level 6 #####
Create a backend that allows games to persist to a database.

##### Level 7 #####
Implement a standard AI for the computer player.  
A standard computer opponent should block a human from winning when able to, but plays more adhoc and 
does not operate with an advanced strategy.

### Bonus ###
##### Boss Level #####
The computer player's decision engine should have a setting for smarter vs standard opponent.  
The smarter version should employ any sort of intelligent strategy or look-ahead approach you choose, 
and be able to win most times (assuming it goes first and it’s not playing against a player with the same strategy), 
though it does not have to play mathematically perfectly. 

### Notes/ Recommendations ###
We won’t judge the UI design, but we do care a lot about user experience.
It is fine to use standard libraries/gems/plugins that go into building a web application, barring anything that literally plays connect 4 for you.
It is also fine to look up known strategies for gameplay, it is your code implementation of that strategy that is the focus.
**Don’t forget to incorporate your understanding of best practices (e.g. DRY, design patterns, etc.)
Make commits often and at various stages to show your train of thought**
