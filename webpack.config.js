'use strict';

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    /*
     * source maps help with mapping transpiled code lines with original source code lines
     * http://cheng.logdown.com/posts/2016/03/25/679045 (list is incomplete though)
     */
    devtool: 'inline-sourcemap' /*'eval-source-map'*/,
    entry: [
        path.join(__dirname, '/src/app/app.js')
    ],
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'app.min.js',
        publicPath: '/'
    },
    plugins: [
        /*
         * OccurrenceOrderPlugin:
         * Assign the module and chunk ids by occurrence count.
         * Ids that are used often get lower (shorter) ids. This make ids predictable, reduces total file size and is recommended.
         */
        new ExtractTextPlugin({
        	filename: "app.min.css",
			allChunks: true
         }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: [ 'react', 'env', 'stage-0'],
                    plugins: [
						'transform-decorators-legacy',
						"react-html-attrs",
                    ]
                }
            },
            {test: /\.(css)$/, loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader'})},
            {test: /\.(sass)$/, loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!sass-loader'})},
        ]
    }
};
