const path = require('path');
const express = require('express');
const webpack = require('webpack');
const config = require('./webpack.config.js');
const webpackMiddleware = require('webpack-dev-middleware');

// check if we are in dev mode. If so, use dev port
const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 9090 : process.env.PORT;

// create express app
const app = express();

// configure webpack and have express use it
const compiler = webpack(config);
const middleware = webpackMiddleware(compiler, {
	publicPath: config.output.publicPath,
	contentBase: 'src',
	stats: {
		colors: true,
		hash: false,
		timings: true,
		chunks: false,
		chunkModules: false,
		modules: false
	}
});
app.use(middleware);

if (isDeveloping) {
	const webpackHotMiddleware = require('webpack-hot-middleware');

	// tell express to use webpack hot reload middleware while we're developing
	app.use(webpackHotMiddleware(compiler));
}

app.get('*', function response(req, res) {
	res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// have express listen on specified port
app.listen(port, function onStart(err) {
	if (err) {
		console.log(err);
	}
	console.info(`==> Running at http://localhost:${port}/`);
});
