import {randomFromRange} from "../utils/common_utils";
import {isNullOrUndefined} from "util";
import Board from "../components/game_specific/Board/Board";
import WinValidator from "../utils/WinValidator";
import PiecePosition from "../components/game_specific/Piece/PiecePosition";
import ConnectFour from "../components/main/ConnectFour";

export class Player {
	name: string;

	constructor(name: string) {
		this.name = name;
	}
}

export class Computer extends Player {
	constructor(name: string) {
		super(name);
	}

	notifyTurn(availableMoves: Array<number>, boardSelections: Array<Array<number>>) {
		return this.makeMove(availableMoves, boardSelections);
	}

	/**
	 * Computer will make move based on the list of available moves. Computer will prioritize any
	 * winning or necessary blocking moves. However, if none are found, a random position will be selected.
	 * @param availableMoves
	 * @param boardSelections
	 * @returns {PiecePosition}
	 */
	makeMove(availableMoves: Array, boardSelections: Array<Array<number>>) {

		// we want to check the list of available moves. Each index represents a board column. If the value for a
		// column is out the range of the board row options, we want to eliminate it as an available move.
		// However, knowing each column is important, so remaining moves are temporarily stored in an array where
		// each value represents their board column index.

		let remainingMoves: Array<PiecePosition> = [];
		let bestMove = null; // keeps track of the best move to make (regarding whether or not this move will block the opponent this turn)
		for(let i = 0; i < availableMoves.length; i++) {
			let row = availableMoves[i];
			if(row < Board.ROWS && availableMoves[i] >= 0) {
				let position = new PiecePosition(row, i);
				// check and see if there is a winning or blocking move
				if (this.checkForWinningMove(position, boardSelections)) {
					// if we can win, we immediately want to do that
					return position;
				} else if (this.checkForBlockingMove(position, boardSelections)) {
					// if we cant win, but we can block, we want to keep this move as a possiblity.
					// we dont want to just block now however, because one of our other available moves
					// can possibly be a winning move.
					bestMove = position;
				}
				//if not we'll add to list of available positions.
				remainingMoves.push(position);
			}
		}
		// randomly select one position
		return isNullOrUndefined(bestMove) ? remainingMoves[randomFromRange(0, remainingMoves.length-1)] : bestMove;
	}

	checkForBlockingMove(position, boardSelections) {
		// If the opposing player is about to win, we want to block them. If we're about to win, we want to select the
		// winning piece
		return WinValidator.hasWinner(position, boardSelections, ConnectFour.PLAYER_1_SLOT);
	}

	checkForWinningMove(position, boardSelections) {
		// If the opposing player is about to win, we want to block them. If we're about to win, we want to select the
		// winning piece
		return WinValidator.hasWinner(position, boardSelections, ConnectFour.PLAYER_2_SLOT);
	}
}
