import React from "react";
import Board from "../game_specific/Board/Board";
import AppBar from "../ui/AppBar/AppBar";
import PiecePosition from "../game_specific/Piece/PiecePosition";
import Piece from "../game_specific/Piece/Piece";
import WinValidator from "../../utils/WinValidator";
import Button from "../ui/Button/Button";
import {isNullOrUndefined} from "util";
import {Player, Computer} from "../../entities/Player";
import {randomFromRange} from "../../utils/common_utils";
import WinnerDialogModal from "../ui/WinnerDialogModal/WinnerDialogModal";

/**
 */
export default class ConnectFour extends React.Component {

	static DISPLAY_NAME = "ConnectFour";

	static EMPTY_SLOT			= -1;	// empty slot value
	static PLAYER_1_SLOT		= 1;	// player 1 slot value
	static PLAYER_2_SLOT		= 2;	// player 2 slot value

	static IN_A_ROW				= 4;

	players = {};


	constructor() {
		super();
		// initialize the game state
		this.state = {
			selections: ConnectFour.initEmptyBoard(),
			turn: this.decideFirstTurnPlayer(),
			availableMoves: ConnectFour.initAvailableMoves(),
			winner: null
		};

		// add players to game
		this.players[ConnectFour.PLAYER_1_SLOT] = new Player("Player 1");
		this.players[ConnectFour.PLAYER_2_SLOT] = new Computer("Computer");
	}

	componentDidMount() {
		if(isNullOrUndefined(this.state.winner)) { this.notifyComputerIfAvailable(); }
	}

	componentDidUpdate() {
		if(isNullOrUndefined(this.state.winner)) { this.notifyComputerIfAvailable(); }
	}

	/**
	 * @returns {Array<Array<number>>} A 2d array representing an empty game board
	 */
	static initEmptyBoard() :Array<Array<number>> {
		const emptyBoard = [];

		for (let i = 0; i < Board.ROWS ; i++) {
			let column = [];
			for (let j = 0; j < Board.COLUMNS ; j++) {
				column.push(ConnectFour.EMPTY_SLOT);
			}
			emptyBoard.push(column);
		}
		return emptyBoard;
	}

	/**
	 * Creates an array, representing the initial available game move.
	 * This will initially be the entire bottom row.
	 * @returns {Array<number>}
	 */
	static initAvailableMoves() :Array<number> {
		/*
		 For each column, the initial available move is the piece located
		 at (column[i], row[last index])
		 */
		let availMoves = [];
		for (let i = 0; i < Board.COLUMNS; i++) {
			availMoves.push(Board.ROWS-1);
		}
		return availMoves;
	}

	/**
	 * Randomly decides which player will go first
	 * @returns {number}
	 */
	decideFirstTurnPlayer() {
		return randomFromRange(ConnectFour.PLAYER_1_SLOT, ConnectFour.PLAYER_2_SLOT);
	}


	determineNextTurn() {
		const currentTurn = this.state.turn;
		return currentTurn === ConnectFour.PLAYER_1_SLOT ? ConnectFour.PLAYER_2_SLOT : ConnectFour.PLAYER_1_SLOT;
	}

	/**
	 * If the computer exists and it is the computer's turn, notify it to make a move.
	 */
	notifyComputerIfAvailable() {
		const {availableMoves, turn, selections} = this.state;
		const turnPlayer: Player = this.players[turn];

		if(turnPlayer instanceof Computer) {

			setTimeout(function() {
				// we'll pass in the array of available moves for current turn. Computer will select and return
				// the index for its selected move
				const selectedPiece = turnPlayer.notifyTurn(availableMoves, selections);
				// update game of selection
				this.onPieceSelected(selectedPiece);
			}.bind(this), 1000);
		}
	}


	/**
	 * Gets the display colour for the piece in question, based its the slotValue.
	 * @param slotValue
	 * @returns {string} Color of piece
	 */
	static getPieceDisplayColor(slotValue: number) :string {
		if (slotValue === ConnectFour.PLAYER_1_SLOT) {
			return Piece.COLOR_PLAYER_1;
		}
		if (slotValue === ConnectFour.PLAYER_2_SLOT) {
			return Piece.COLOR_PLAYER_2;
		}
		if (slotValue === ConnectFour.EMPTY_SLOT) {
			return Piece.COLOR_EMPTY;
		} else {
			console.error(ConnectFour.DISPLAY_NAME, "getDisplayColor()", `slotValue = ${slotValue}`);
			return Piece.COLOR_EMPTY;
		}
	}

	onPieceSelected(position: PiecePosition) {
		// update the game state with the new selection.
		const updatedSelections = this.state.selections.concat();
		const nextTurn = this.determineNextTurn();
		const availableMoves = this.state.availableMoves.concat();

		// add new selection to board. Selection is represented by the current turn's player's slot value.
		updatedSelections[position.row][position.column] = this.state.turn;

		const hasWinner = (WinValidator.hasWinner(position, this.state.selections.concat(), this.state.turn));

		// update list of available moves. The next row in the current selection's column will now be available.
		availableMoves[position.column] = position.row-1;
		// update game state
		this.setState({
			selections: updatedSelections,
			turn: nextTurn,
			availableMoves: availableMoves,
			winner: hasWinner ? this.state.turn : null
		});
	}

	/**
	 * resets the game state, emptying the board
	 */
	restartGame() {
		this.setState({
			selections: ConnectFour.initEmptyBoard(),
			turn: this.decideFirstTurnPlayer(),
			availableMoves: ConnectFour.initAvailableMoves(),
			winner: null
		});
	}

	render() {
		const {availableMoves, turn, selections, winner} = this.state;

		// display a winning dialog only if a winner has been selected
		const winnerDialog = isNullOrUndefined(winner) ? "" : (
			<WinnerDialogModal winner={this.players[winner].name} onNewGameClickListener={this.restartGame.bind(this)}/>
			);

		return (
			<div className={`${ConnectFour.DISPLAY_NAME}`}>
				<AppBar onNewGameSelectedListener={this.restartGame.bind(this)}/>
				<h3>{`It's ${this.players[turn].name}'s turn!`}</h3>
				<Board availableMoves={availableMoves}
					   onPieceClickListener={this.onPieceSelected.bind(this)}
					   selections={selections} selectable={turn === ConnectFour.PLAYER_1_SLOT}/>
				{winnerDialog}
			</div>
		);
	}
}
