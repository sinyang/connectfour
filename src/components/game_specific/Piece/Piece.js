import React from "react";
import PiecePosition from "./PiecePosition";

/**
 */
export default class Piece extends React.Component {

	static DISPLAY_NAME		= 'Piece';

	// game piece colors
	static COLOR_EMPTY		= "yellow";
	static COLOR_PLAYER_1	= "red";
	static COLOR_PLAYER_2	= "blue";

	static propTypes = {
		color:					React.PropTypes.string,
		isAvailable:			React.PropTypes.bool,
		onPieceClickListener:	React.PropTypes.func,
		position:				React.PropTypes.instanceOf(PiecePosition).isRequired,
	};

	onClick() {
		// send piece position back to board for processing
		this.props.onPieceClickListener(this.props.position);
	}

	render() {
		const {color, isAvailable, position} = this.props;
		const availableClass = isAvailable ? "available" : "";
		return (
			<div className={`${Piece.DISPLAY_NAME} ${color}-backgrounded ${availableClass}`} onClick={this.onClick.bind(this)}>
				{/*{position.row}, {position.column}*/}
			</div>
		);
	}
}
