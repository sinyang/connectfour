export default class PiecePosition {
	column:	number;
	row:	number;

	/**
	 * Coordinates for a single piece on game board
	 * @param row: number
	 * @param column: number
	 */
	constructor(row:number, column:number) {
		this.column	= column;
		this.row	= row;
	}
}