import React from "react";
import Piece from "../Piece/Piece";
import PiecePosition from "../Piece/PiecePosition"
import ConnectFour from "../../main/ConnectFour";

/**
 */
export default class Board extends React.Component {
	static DISPLAY_NAME 	= "Board";

	// The most commonly used Connect Four board size is 7 columns × 6 rows.
	static COLUMNS			= 7;
	static ROWS				= 6;

	static propTypes = {
		availableMoves:			React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
		onPieceClickListener:	React.PropTypes.func,
		selections:				React.PropTypes.arrayOf(React.PropTypes.arrayOf(React.PropTypes.number)).isRequired,
		selectable:				React.PropTypes.bool
	};

	static defaultProps = {
		onPieceClickListener:	function(){},
	};

	constructor() {
		super();
	}

	getBoardPieces(): Array<Piece> {
		// We'll achieve this by making an array of Piece components.
		// The Board is a flex box that will wrap the 1D array pieces to new rows.

		const usersSelections	:Array<Array<number>>	= this.props.selections;	// current board piece selections
		let pieces				:Array<Piece>			= [];						// array of DOM piece elements

		let slotValue: number;			// value representing the player that has selected this piece, if at all
		let displayColor: string;		// color representing the player that has selected this piece
		let position: PiecePosition;	// [row, column] coordinates for the current piece
		let isAvailable: boolean;		// boolean representing whether or not the piece is available for selection
		for (let i = 0; i < Board.ROWS ; i++) {
			for (let j = 0; j < Board.COLUMNS; j++) {
				slotValue = usersSelections[i][j];
				displayColor = ConnectFour.getPieceDisplayColor(slotValue);
				position = new PiecePosition(i, j);
				isAvailable = this.isPieceAvailable(position);
				pieces.push(
					<Piece key={pieces.length} position={position}
						   color={displayColor} isAvailable={isAvailable}
						   onPieceClickListener={this.onPieceClick.bind(this)}/>
				);
			}
		}
		return pieces;
	}

	/**
	 * Determines if the piece at the passed in position parameter is available to be selected.
	 * @param position : (row,column) coordinates for the piece in question
	 * @returns {boolean} : True if the piece is available
	 */
	isPieceAvailable(position: PiecePosition) {
		// check the array of available moves. Each index represents a column.
		// Each value represents a row for that column
		return position.row === this.props.availableMoves[position.column]
	}

	onPieceClick(position: PiecePosition) {
		if (this.isPieceAvailable(position)) {
			this.props.onPieceClickListener(position);
		}
	}


	render() {
		const disabledClass = !this.props.selectable ? "disabled" : "";
		return (
			<div className={`${Board.DISPLAY_NAME} ${disabledClass}`}>
				{this.getBoardPieces()}
			</div>
		);
	}
}

Board.SLOTS	= Board.COLUMNS * Board.ROWS;