import React from "react";
import Button from "../Button/Button";

/**
 */
export default class WinnerDialogModal extends React.Component {

	static propTypes = {
		winner:					React.PropTypes.string,
		onNewGameClickListener:	React.PropTypes.func,
	};

	static defaultProps = {
		winner:					"",
		onNewGameClickListener:	function(){},
	};

	constructor() {
		super();
	}

	render() {
		return (
			<div className="WinnerDialogModal">
				<div className="winner-dialog">
					<div><span className="red-text-colored">{this.props.winner}</span> has won!</div>
					<Button className={'blue-backgrounded white-text-colored'}
							onClickListener={this.props.onNewGameClickListener}>
						Play Again
					</Button>
				</div>
			</div>
		);
	}
}
