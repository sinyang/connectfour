import React from "react"

export default class Button extends React.Component {

	static DISPLAY_NAME	= "Button";

	static propTypes = {
		className:			React.PropTypes.string,
		onClickListener:	React.PropTypes.func,
	};

	static defaultProps = {
		className:			"",
		onClickListener:	function() {},
	};

	render() {
		return (
			<button className={`${this.props.className} ${Button.DISPLAY_NAME}`} onClick={this.props.onClickListener}>
				{this.props.children}
			</button>
		);
	}
}
