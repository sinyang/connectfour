import React from "react";
import Button from "../Button/Button";

/**
 */
export default class AppBar extends React.Component {
	static DISPLAY_NAME = "AppBar";

	static propTypes = {
		onNewGameSelectedListener:	React.PropTypes.func,
	};

	static defaultProps = {
		onNewGameSelectedListener:	function(){},
	};

	constructor() {
		super();
	}

	render() {
		return (
			<div className={`${AppBar.DISPLAY_NAME}`}>
				<div className="app-name red-text-colored"><h1>Connect Four</h1></div>
				<div>
					<Button onClickListener={this.props.onNewGameSelectedListener} className={'blue-backgrounded white-text-colored'}>New</Button>
					{/*<Button className={'blue-text-colored'}>Save</Button>*/}
					{/*<Button className={'blue-text-colored'}>Load</Button>*/}
				</div>
			</div>
		);
	}
}
