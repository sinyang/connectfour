import PiecePosition from "../components/game_specific/Piece/PiecePosition";
import ConnectFour from "../components/main/ConnectFour";
import Board from "../components/game_specific/Board/Board";
export default class WinValidator {
	static hasWinner(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		return WinValidator.isHorizontalWin(position, selections, currPlayerSlotValue) ||
			WinValidator.isVerticalWin(position, selections, currPlayerSlotValue) ||
			WinValidator.isDiagonalWin(position, selections, currPlayerSlotValue);
	}

	static isHorizontalWin(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		return (1 + WinValidator.getLeftMatchCount(position, selections, currPlayerSlotValue)
			+ WinValidator.getRightMatchCount(position, selections, currPlayerSlotValue)) >= ConnectFour.IN_A_ROW;
	}


	static isVerticalWin(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		return (1 + WinValidator.getTopMatchCount(position, selections, currPlayerSlotValue)
			+ WinValidator.getBottomMatchCount(position, selections, currPlayerSlotValue)) >= ConnectFour.IN_A_ROW;
	}

	static isDiagonalWin(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		return (
			(1 + WinValidator.getBottomLeftMatchCount(position, selections, currPlayerSlotValue)
			+ WinValidator.getTopRightMatchCount(position, selections, currPlayerSlotValue)) >= ConnectFour.IN_A_ROW ||
			(1 + WinValidator.getBottomRightMatchCount(position, selections, currPlayerSlotValue)
			+ WinValidator.getTopLeftMatchCount(position, selections, currPlayerSlotValue)) >= ConnectFour.IN_A_ROW
		);
	}


	static getLeftMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		let consecutiveMatches = 0;
		for(let i = position.column-1; i >= 0; i--) {
			if(selections[position.row][i] === currPlayerSlotValue) {
				consecutiveMatches++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}
	static getRightMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		let consecutiveMatches = 0;
		for(let i = position.column+1; i < Board.COLUMNS; i++) {
			if(selections[position.row][i] === currPlayerSlotValue) {
				consecutiveMatches++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getTopMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue){
		let consecutiveMatches = 0;
		for(let i = position.row-1; i >= 0; i--) {
			if(selections[i][position.column] === currPlayerSlotValue) {
				consecutiveMatches++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getBottomMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		let consecutiveMatches = 0;
		for(let i = position.row+1; i < Board.ROWS; i++) {
			if(selections[i][position.column] === currPlayerSlotValue) {
				consecutiveMatches++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getBottomLeftMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue){
		let consecutiveMatches = 0;
		let nextDiagRow = position.row-1;
		let nextDiagCol = position.column-1;
		while (nextDiagRow >= 0 && nextDiagCol >= 0) {
			if(selections[nextDiagRow][nextDiagCol] === currPlayerSlotValue) {
				consecutiveMatches++;
				nextDiagRow--;
				nextDiagCol--;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getTopRightMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		let consecutiveMatches = 0;
		let nextDiagRow = position.row+1;
		let nextDiagCol = position.column+1;
		while (nextDiagRow < Board.ROWS && nextDiagCol < Board.COLUMNS) {
			if(selections[nextDiagRow][nextDiagCol] === currPlayerSlotValue) {
				consecutiveMatches++;
				nextDiagRow++;
				nextDiagCol++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getBottomRightMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue){
		let consecutiveMatches = 0;
		let nextDiagRow = position.row-1;
		let nextDiagCol = position.column+1;
		while (nextDiagRow >= 0 && nextDiagCol < Board.COLUMNS) {
			if(selections[nextDiagRow][nextDiagCol] === currPlayerSlotValue) {
				consecutiveMatches++;
				nextDiagRow--;
				nextDiagCol++;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

	static getTopLeftMatchCount(position: PiecePosition, selections: Array<Array<number>>, currPlayerSlotValue) {
		let consecutiveMatches = 0;
		let nextDiagRow = position.row+1;
		let nextDiagCol = position.column-1;
		while (nextDiagRow < Board.ROWS && nextDiagCol >= 0) {
			if(selections[nextDiagRow][nextDiagCol] === currPlayerSlotValue) {
				consecutiveMatches++;
				nextDiagRow++;
				nextDiagCol--;
			} else {
				break;
			}
		}
		return consecutiveMatches;
	}

}