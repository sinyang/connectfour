import React from 'react';
import ReactDOM from 'react-dom';
import "normalize.css"
import './app.sass';
import ConnectFour from "../components/main/ConnectFour";

/*
 TODO
 look into the following warning, possibly caused by a dependency of a dependency:
 (node:7624) DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic, see
 https://github.com/webpack/loader-utils/issues/56
 parseQuery() will be replaced with getOptions() in the next major version of loader-utils.
*/

// render react application to html dom
ReactDOM.render(<ConnectFour/>, document.getElementById('application'));
